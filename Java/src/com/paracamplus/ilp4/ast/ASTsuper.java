/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp4.ast;

import com.paracamplus.ilp1.ast.ASTexpression;
import com.paracamplus.ilp4.interfaces.IASTsuper;
import com.paracamplus.ilp4.interfaces.IASTvisitor;

public class ASTsuper extends ASTexpression implements IASTsuper {
	public ASTsuper(int line) {
		this.line=line;
		
	}
    private int line;
    @Override
    public <Result, Data, Anomaly extends Throwable> Result accept(
			com.paracamplus.ilp1.interfaces.IASTvisitor<Result, Data, Anomaly> visitor,
			Data data) throws Anomaly {
        return ((IASTvisitor<Result, Data, Anomaly>) visitor).visit(this, data);
    }
	@Override
	public int getLine() {
		return line;
	}
}
