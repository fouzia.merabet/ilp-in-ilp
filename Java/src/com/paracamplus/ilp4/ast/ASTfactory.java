/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp4.ast;

import com.paracamplus.ilp4.interfaces.IASTclassDefinition;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.interfaces.IASTfieldRead;
import com.paracamplus.ilp4.interfaces.IASTfieldWrite;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp4.interfaces.IASTfactory;
import com.paracamplus.ilp4.interfaces.IASTinstantiation;
import com.paracamplus.ilp4.interfaces.IASTmethodDefinition;
import com.paracamplus.ilp4.interfaces.IASTprogram;
import com.paracamplus.ilp4.interfaces.IASTself;
import com.paracamplus.ilp4.interfaces.IASTsend;
import com.paracamplus.ilp4.interfaces.IASTsuper;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public class ASTfactory 
extends  com.paracamplus.ilp3.ast.ASTfactory
implements IASTfactory {

    @Override
	public IASTprogram newProgram(int line,IASTfunctionDefinition[] functions,
                                  IASTclassDefinition[] clazzes, 
                                  IASTexpression expression) {
        return new ASTprogram(line,functions, clazzes, expression);
    }
    

    
    @Override
	public IASTclassDefinition newClassDefinition(
			int line,
            String className,
            String superClassName, 
            String[] fieldNames,
            IASTmethodDefinition[] methodDefinitions) {
        return new ASTclassDefinition(
        		line,
                className,
                superClassName,
                fieldNames,
                methodDefinitions );
    }

    @Override
	public IASTmethodDefinition newMethodDefinition(
			int line,
            IASTvariable methodVariable,
            IASTvariable[] variables, 
            IASTexpression body,
            String methodName,
            String definingClassName ) {
        return new ASTmethodDefinition(
        		line,
        		methodVariable, variables, body, 
                methodName, definingClassName);
    }

    @Override
	public IASTinstantiation newInstantiation(
			int line,
            String className,
            IASTexpression[] arguments) {
        return new ASTinstantiation(line,className, arguments);
    }

    @Override
	public IASTfieldRead newReadField(int line,String fieldName, IASTexpression target) {
        return new ASTfieldRead(line,fieldName, target);
    }

    @Override
	public IASTfieldWrite newWriteField(
			int line,
            String fieldName,
            IASTexpression target, 
            IASTexpression value) {
        return new ASTfieldWrite(line,fieldName, target, value);
    }

    @Override
	public IASTsend newSend(
			int line,
            String message, 
            IASTexpression receiver,
            IASTexpression[] arguments) {
        return new ASTsend(line,message, receiver, arguments);
    }

    @Override
	public IASTself newSelf(int line) {
        return new ASTself(line);
    }

    @Override
	public IASTsuper newSuper(int line) {
        return new ASTsuper(line);
    }
}
