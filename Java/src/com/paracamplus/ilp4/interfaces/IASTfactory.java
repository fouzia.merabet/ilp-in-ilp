/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp4.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTfactory extends com.paracamplus.ilp3.interfaces.IASTfactory{
    IASTprogram newProgram(
    		int line,
    		IASTfunctionDefinition[] functions,
    		IASTclassDefinition[] clazzes,
            IASTexpression expression);
    

    IASTclassDefinition newClassDefinition(
    		int line,
            String className,
            String superClassName,
            String[] fieldNames,
            IASTmethodDefinition[] methodDefinitions );
    
    IASTmethodDefinition newMethodDefinition(
    		int line,
            IASTvariable methodVariable,
            IASTvariable[] variables,
            IASTexpression body, 
            String methodName,
            String definingClassName  );
    
    IASTexpression newInstantiation(
    		int line,
            String className,
            IASTexpression[] arguments );
    
    IASTexpression newReadField(
    		int line,
            String fieldName,
            IASTexpression object );
    
    IASTexpression newWriteField(
    		int line,
            String fieldName,
            IASTexpression object,
            IASTexpression value );
    
    IASTvariable newSelf(int line);
    
    IASTexpression newSend(
    		int line,
            String message,
            IASTexpression receiver,
            IASTexpression[] arguments );
    
    IASTexpression newSuper(int line);
}
