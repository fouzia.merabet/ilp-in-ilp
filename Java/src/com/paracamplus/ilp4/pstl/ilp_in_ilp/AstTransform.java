package com.paracamplus.ilp4.pstl.ilp_in_ilp;

import com.paracamplus.ilp1.interfaces.IASTalternative;
import com.paracamplus.ilp1.interfaces.IASTbinaryOperation;
import com.paracamplus.ilp1.interfaces.IASTblock;
import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTboolean;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTfloat;
import com.paracamplus.ilp1.interfaces.IASTinteger;
import com.paracamplus.ilp1.interfaces.IASTinvocation;
import com.paracamplus.ilp1.interfaces.IASTprogram;
import com.paracamplus.ilp1.interfaces.IASTsequence;
import com.paracamplus.ilp1.interfaces.IASTstring;
import com.paracamplus.ilp1.interfaces.IASTunaryOperation;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.interfaces.IASTvisitor;
import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;

public class AstTransform implements IASTvisitor<String,Void,EvaluationException>{

	
	private String prefixTemplate = 
		"import SetImports.ilpml\n"+
		"let opStuff = new OperatorStuff(new ListPair(new Empty()),new ListPair(new Empty())) in \n" + 
		"\n" + 
		"        (\n" + 
		"           opStuff.setBinOpEnv(opStuff.initBinOp());\n" + 
		"           opStuff.setUnaryOpEnv(opStuff.initUnaryOp());\n" + 
		"           \n"; 
		
	private String suffixTemplate = 
		"           \n" + 
		"           in (x.compile(x.visitGlobalVar(new GlobalVariableEnvironment(new Empty())),new Context(new NoDestination(),0),opStuff))\n" + 
		"         )";

	public String visit (IASTprogram iast, Void data) 
            throws EvaluationException {
		StringBuilder stdout = new StringBuilder();
		stdout.append(prefixTemplate);
		stdout.append("           let x = new ASTProgram(");
        stdout.append(iast.getBody().accept(this, data));
        stdout.append(")\n");
        stdout.append(suffixTemplate);
            return stdout.toString();
    }
	
	
	@Override
	public String visit(IASTalternative iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		stdout.append("new ASTalternative(");
		stdout.append(iast.getCondition().accept(this, data));
		stdout.append(",");
		stdout.append(iast.getConsequence().accept(this,data));
		stdout.append(",");
		if(iast.isTernary())
			stdout.append(iast.getAlternant().accept(this,data));
		else
			stdout.append("false)");
		return stdout.toString();
	}

	@Override
	public String visit(IASTbinaryOperation iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		stdout.append("new ASTBinOperation(");
		stdout.append("\""+iast.getOperator().getName()+"\"");
		stdout.append(",");
		stdout.append(iast.getLeftOperand().accept(this,data));
		stdout.append(",");
		stdout.append(iast.getRightOperand().accept(this,data));
		stdout.append(")");
		return stdout.toString();
	}

	@Override
	public String visit(IASTblock iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		IASTbinding[] bd = iast.getBindings();
		stdout.append("new ASTBlock(");
		stdout.append("new ASTBinding(");
		for (int i = 0; i < bd.length; i++) {
			stdout.append("new Cons(");
			stdout.append("new Pair(");
			stdout.append(bd[i].getVariable().accept(this,data));
			stdout.append(",");
			stdout.append(bd[i].getInitialisation().accept(this,data));
			stdout.append(")");
			stdout.append(",");
		}
		stdout.append("new Empty()))");
		stdout.append(",");
		stdout.append(iast.getBody().accept(this,data));
		stdout.append(")");
		return stdout.toString();
	}

	@Override
	public String visit(IASTboolean iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		stdout.append("new ASTBoolean(");
		stdout.append(iast.getValue());
		stdout.append(")");
		return stdout.toString();
	}

	@Override
	public String visit(IASTfloat iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		stdout.append("new ASTFloat(");
		stdout.append(iast.getValue());
		stdout.append(")");
		return stdout.toString();
	}

	@Override
	public String visit(IASTinteger iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		stdout.append("new ASTInteger(");
		stdout.append(iast.getValue());
		stdout.append(")");
		return stdout.toString();
	}

	@Override
	public String visit(IASTinvocation iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		return "";
	}

	@Override
	public String visit(IASTsequence iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		IASTexpression[] exprs = iast.getExpressions();
		stdout.append("new ASTSequence(");
		for (int i = 0; i < exprs.length; i++) {
			stdout.append("new Cons(");
			stdout.append(exprs[i].accept(this, data));
			stdout.append(",");
		}
		
		stdout.append("new Empty()))");
		return stdout.toString();
	}

	@Override
	public String visit(IASTstring iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		stdout.append("new ASTString(\"");
		stdout.append(iast.getValue());
		stdout.append("\")");
		return stdout.toString();
	}

	@Override
	public String visit(IASTunaryOperation iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		stdout.append("new ASTUnaryOperation(");
		stdout.append("\""+iast.getOperator().getName()+"\"");
		stdout.append(",");
		stdout.append(iast.getOperand().accept(this,data));
		stdout.append(")");
		return stdout.toString();
	}

	@Override
	public String visit(IASTvariable iast, Void data) throws EvaluationException {
		// TODO Auto-generated method stub
		StringBuilder stdout = new StringBuilder();
		stdout.append("new ASTVar(");
		stdout.append(iast.getName());
		stdout.append(")");
		return stdout.toString();
	}

}
