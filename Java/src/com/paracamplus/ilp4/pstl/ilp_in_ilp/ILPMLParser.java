package com.paracamplus.ilp4.pstl.ilp_in_ilp;

import java.io.FileOutputStream;

import org.antlr.v4.runtime.ANTLRInputStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import antlr4.ILPMLgrammar4Lexer;
import antlr4.ILPMLgrammar4Parser;

import com.paracamplus.ilp4.interfaces.IASTfactory;
import com.paracamplus.ilp4.interfaces.IASTprogram;
import com.paracamplus.ilp4.parser.ilpml.ILPMLListener;
import com.paracamplus.ilp1.parser.ParseException;
public class ILPMLParser extends com.paracamplus.ilp4.parser.ilpml.ILPMLParser{
	
	public static int cntFiles = 0;
	
	public ILPMLParser(IASTfactory factory) {
		super(factory);
	}
	
    public IASTprogram getProgram() throws ParseException {
		try {
			//transformation du code ilp en AST DLP
			ANTLRInputStream in = new ANTLRInputStream(input.getText());
			// flux de caractères -> analyseur lexical
			ILPMLgrammar4Lexer lexer = new ILPMLgrammar4Lexer(in);
			// analyseur lexical -> flux de tokens
			CommonTokenStream tokens =	new CommonTokenStream(lexer);
			// flux tokens -> analyseur syntaxique
			ILPMLgrammar4Parser parser =	new ILPMLgrammar4Parser(tokens);
			// démarage de l'analyse syntaxique
			ILPMLgrammar4Parser.ProgContext tree = parser.prog();		
			// parcours de l'arbre syntaxique et appels du Listener
			ParseTreeWalker walker = new ParseTreeWalker();
			ILPMLListener extractor = new ILPMLListener((IASTfactory)factory);
			walker.walk(extractor, tree);
			///////
			
			/*transformation de l'AST DLP en AST ILP IN ILP et 
			 * creation du fichier d'execution du compilateur ILP in ILP
			 */
			
			String prog = (new AstTransform()).visit(tree.node, null);
			
			FileOutputStream fos = new FileOutputStream("/Users/hakimbaaloudj/pstl/ilp-upmc/Bootstrap/tests/u"+cntFiles+"-1.ilpml");
			fos.write(prog.getBytes());
			fos.flush();
			fos.close();
			cntFiles++;
			
			/*System.out.println(prog);
			in = new ANTLRInputStream(prog);
			lexer = new ILPMLgrammar4Lexer(in);
			tokens =	new CommonTokenStream(lexer);
			parser =	new ILPMLgrammar4Parser(tokens);
			tree = parser.prog();
			walker.walk(extractor, tree);*/
			
			
			return tree.node;
		} catch (Exception e) {
			throw new ParseException(e);
		}
    }

}

