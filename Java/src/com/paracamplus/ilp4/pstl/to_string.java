package com.paracamplus.ilp4.pstl;


import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.UnaryPrimitive;

public class to_string extends UnaryPrimitive {
    
	public to_string() {
	    super("to_string");
	}
	@Override
	public Object apply (Object value) throws EvaluationException {
		String s =String.valueOf(value);
	                return s ;
	       
		}
		

}
