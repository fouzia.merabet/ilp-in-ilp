package com.paracamplus.ilp4.pstl;


import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.UnaryPrimitive;

public class string_to_int extends UnaryPrimitive {
    
	public string_to_int() {
	    super("string_to_int");
	}
	@Override
	public Object apply (Object value) throws EvaluationException {
		if (value instanceof String)
            return  Integer.parseInt( (String) value);
   
	        else throw new EvaluationException("Invalid argument: number expected");
		}
		

}

