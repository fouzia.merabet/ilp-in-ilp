
package com.paracamplus.ilp4.pstl.compiler;


import com.paracamplus.ilp1.compiler.interfaces.IGlobalVariableEnvironment;
import com.paracamplus.ilp1.compiler.interfaces.IOperatorEnvironment;



public class Compiler extends com.paracamplus.ilp4.compiler.Compiler {
    
 
    public Compiler(IOperatorEnvironment ioe, IGlobalVariableEnvironment igve) {
		super(ioe, igve);
		cProgramPrefix = ""
	            + "#include <stdio.h> \n"
	            + "#include <stdlib.h> \n"
	            + "#include \"ilp.h\" \n\n"
	    		+ "#include \"ilplength.h\" \n"
	    		+ "#include \"ilplength.c\"\n"
	    		+ "#include \"ilpstrinftoint.h\" \n"
	    		+ "#include \"ilpstringtoint.c\"\n"
	    		+ "#include \"ilptostring.h\" \n"
	    		+ "#include \"ilptostring.c\"\n"
	    		;
	}
    
    
}



