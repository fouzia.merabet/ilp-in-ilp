package com.paracamplus.ilp4.pstl;


import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;
import com.paracamplus.ilp1.interpreter.primitive.UnaryPrimitive;

public class length extends UnaryPrimitive {
    
	public length() {
	    super("length");
	}
	@Override
	public Object apply (Object value) throws EvaluationException {
		String  a = (String)value;
	                return a.length()  ;
	       
		}
		

}
