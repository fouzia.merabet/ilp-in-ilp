/* *****************************************************************
 * ilp4 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp4
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp4.compiler.normalizer;

import com.paracamplus.ilp4.compiler.interfaces.IASTCclassDefinition;
import com.paracamplus.ilp2.compiler.interfaces.IASTCfunctionDefinition;
import com.paracamplus.ilp4.compiler.interfaces.IASTCmethodDefinition;
import com.paracamplus.ilp4.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalFunctionVariable;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface INormalizationFactory 
extends com.paracamplus.ilp3.compiler.normalizer.INormalizationFactory {

    IASTCprogram newProgram(int line,
    		                IASTCfunctionDefinition[] functions,
                            IASTCclassDefinition[] clazzes, 
                            IASTexpression expression);

    
    // Class related
    
    IASTCglobalFunctionVariable newMethodVariable(int line,String name);
    
     IASTCclassDefinition newClassDefinition(
    		 int line,
            String className,
            IASTCclassDefinition superClass, 
            String[] fieldNames,
            IASTCmethodDefinition[] methodDefinitions);

     IASTCmethodDefinition newMethodDefinition(
    		 int line,
             IASTvariable methodVariable,
             IASTvariable[] variables, 
             IASTexpression body,
             String methodName,
             IASTCclassDefinition definingClass );
     
     IASTexpression newInstantiation(int line,
    		                         IASTCclassDefinition clazz,
                                     IASTexpression[] arguments);

     IASTexpression newReadField(int line,
    		                     IASTCclassDefinition clazz,
                                 String fieldName, 
                                 IASTexpression target);

     IASTexpression newWriteField(int line,
    		                      IASTCclassDefinition clazz,
                                  String fieldName,
                                  IASTexpression target, 
                                  IASTexpression value);

     IASTvariable newSelf(int line);

     IASTexpression newSend(int line,
    		                String message, 
                            IASTexpression receiver,
                            IASTexpression[] arguments);
    
     IASTexpression newSuper(int line);
}
