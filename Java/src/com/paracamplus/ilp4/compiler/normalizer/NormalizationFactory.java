/* *****************************************************************
 * ilp4 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp4
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp4.compiler.normalizer;


import com.paracamplus.ilp4.ast.ASTself;
import com.paracamplus.ilp4.ast.ASTsend;
import com.paracamplus.ilp4.ast.ASTsuper;
import com.paracamplus.ilp4.compiler.ast.ASTCclassDefinition;
import com.paracamplus.ilp4.compiler.ast.ASTCfieldRead;
import com.paracamplus.ilp4.compiler.ast.ASTCfieldWrite;
import com.paracamplus.ilp4.compiler.ast.ASTCinstantiation;
import com.paracamplus.ilp4.compiler.ast.ASTCmethodDefinition;
import com.paracamplus.ilp4.compiler.ast.ASTCprogram;
import com.paracamplus.ilp4.compiler.interfaces.IASTCclassDefinition;
import com.paracamplus.ilp4.compiler.interfaces.IASTCfieldRead;
import com.paracamplus.ilp4.compiler.interfaces.IASTCfieldWrite;
import com.paracamplus.ilp2.compiler.ast.ASTCglobalFunctionVariable;
import com.paracamplus.ilp2.compiler.interfaces.IASTCfunctionDefinition;
import com.paracamplus.ilp4.compiler.interfaces.IASTCinstantiation;
import com.paracamplus.ilp4.compiler.interfaces.IASTCmethodDefinition;
import com.paracamplus.ilp4.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalFunctionVariable;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp4.interfaces.IASTself;
import com.paracamplus.ilp4.interfaces.IASTsend;
import com.paracamplus.ilp4.interfaces.IASTsuper;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public class NormalizationFactory
extends com.paracamplus.ilp3.compiler.normalizer.NormalizationFactory
implements INormalizationFactory {
    
  
    @Override
	public IASTCprogram newProgram(
			int line,
            IASTCfunctionDefinition[] functions,
            IASTCclassDefinition[] clazzes, 
            IASTexpression expression) {
        return new ASTCprogram(line,functions, clazzes, expression); 
    }

 
    // Class related
    
    @Override
	public IASTCglobalFunctionVariable newMethodVariable(int line,String methodName) {
        String newName = methodName + "_" + count.incrementAndGet();
        return new ASTCglobalFunctionVariable(line,newName);
    }

    @Override
	public IASTCclassDefinition newClassDefinition(
			int line,
            String className,
            IASTCclassDefinition superClass, 
            String[] fieldNames,
            IASTCmethodDefinition[] methodDefinitions) {
        return new ASTCclassDefinition(
        		line,
                className,
                superClass,
                fieldNames,
                methodDefinitions );
    }

    @Override
	public IASTCmethodDefinition newMethodDefinition(
			int line,
            IASTvariable methodVariable,
            IASTvariable[] variables, 
            IASTexpression body,
            String methodName,
            IASTCclassDefinition definingClass ) {
        return new ASTCmethodDefinition(
                line,methodVariable, variables, body, methodName, definingClass);
    }

    @Override
	public IASTCinstantiation newInstantiation(
			int line,
            IASTCclassDefinition clazz,
            IASTexpression[] arguments) {
        return new ASTCinstantiation(line,clazz, arguments);
    }

    @Override
	public IASTCfieldRead newReadField(
			int line,
            IASTCclassDefinition clazz,
            String fieldName, 
            IASTexpression target) {
        return new ASTCfieldRead(line,clazz, fieldName, target);
    }

    @Override
	public IASTCfieldWrite newWriteField(
			int line,
            IASTCclassDefinition clazz,
            String fieldName,
            IASTexpression target, 
            IASTexpression value) {
        return new ASTCfieldWrite(line,clazz, fieldName, target, value);
    }

    @Override
	public IASTsend newSend(
			int line,
            String message, 
            IASTexpression receiver,
            IASTexpression[] arguments) {
        return new ASTsend(line,message, receiver, arguments);
    }

    @Override
	public IASTself newSelf(int line) {
        return new ASTself(line);
    }

    @Override
	public IASTsuper newSuper(int line) {
        return new ASTsuper(line);
    }
}
