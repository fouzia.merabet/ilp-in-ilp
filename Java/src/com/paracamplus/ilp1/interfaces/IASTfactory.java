/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp1.interfaces;

import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;


public interface IASTfactory {
    IASTprogram newProgram(
    	    int line,
            IASTexpression expression);
    
    IASTexpression newSequence(int line,IASTexpression[] asts);

    IASTexpression newAlternative(
    	    int line,
            IASTexpression condition,
            IASTexpression consequence,
            IASTexpression alternant);

    IASToperator newOperator(String name);
    
    IASTvariable newVariable(String name);
    

    IASTexpression newUnaryOperation(
    	    int line,
            IASToperator operator,
            IASTexpression operand);

    IASTexpression newBinaryOperation(
    	    int line,
            IASToperator operator,
            IASTexpression leftOperand,
            IASTexpression rightOperand);

    IASTexpression newIntegerConstant(String value);

    IASTexpression newFloatConstant(String value);

    IASTexpression newStringConstant(String value);

    IASTexpression newBooleanConstant(String value);


    IASTexpression newBlock(int line,
    						IASTbinding[] binding,
                            IASTexpression body);

    IASTbinding newBinding(int line,IASTvariable v, IASTexpression exp);
    
    IASTexpression newInvocation(
    		int line,
            IASTexpression function,
            IASTexpression[] arguments);

	
   
}
