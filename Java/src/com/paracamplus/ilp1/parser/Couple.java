package com.paracamplus.ilp1.parser;

public class Couple {
	
	public String getFile() {
		return file;
	}
	
	public void setFile(String file) {
		this.file=file;
	}
	
	public void setLine(int line) {
		this.line=line;
	}
	
	public int getLine() {
		return line;
	}
	
	public Couple(String file, int line) {
		super();
		this.file=file;
		this.line=line;
		
	}
	
	private String file;
	private int line;
}
