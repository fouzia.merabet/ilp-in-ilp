package com.paracamplus.ilp1.parser;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Scanner;

import com.paracamplus.ilp1.interfaces.IASTprogram;
import com.paracamplus.ilp1.parser.ilpml.ILPMLParser;
import com.paracamplus.ilp1.parser.xml.IXMLParser;
import com.paracamplus.ilp1.tools.Input;
import com.paracamplus.ilp1.tools.InputFromFile;

/*
 * Dispatch vers le bon parseur, en fonction de l'extension de fichier.
 */
public class Parser {

    public int index=1;
    public int x=0;
    public int y=0;
   
    public static HashMap<Integer,Couple> map = new HashMap<Integer,Couple>();
   
    public Parser() {
    }

    protected IXMLParser xmlparser;

    public void setXMLParser(IXMLParser xMLParser) {
        this.xmlparser = xMLParser;
    }

    protected ILPMLParser ilpmlparser;

    public void setILPMLParser(ILPMLParser parser) {
        this.ilpmlparser = parser;
    }

    private List<String> importRec(File fileMaster) throws FileNotFoundException {
        Scanner sc = new Scanner(fileMaster);
        List<String> progMaster = new ArrayList<>();
        while (sc.hasNextLine()) {
            String line = sc.nextLine();
          
            
            if (line.contains("import")) {
               
                String[] cps = line.split(" ");
              //  x=1;
                
                List<String> progSlave = importRec(new File(fileMaster.getParentFile()+"/"+ cps[1]));
                
                progMaster.addAll(progSlave);
                x=1;
            } else{
            	  x++;
            	
                map.put(index,new Couple(fileMaster.getName(),x));
               
                
                progMaster.add(line);
                index++;
            }
        }
        sc.close();
        return progMaster;
    }

    private File importFiles(File fileMaster) throws FileNotFoundException {
        List<String> progMaster = importRec(fileMaster);
        File ret = new File("result.ilpml");
       
        PrintWriter pw = new PrintWriter(ret);
        for (String line : progMaster)
            pw.println(line);
        pw.close();
       
        return ret;
    }

    public IASTprogram parse(File f) throws ParseException, FileNotFoundException {

        File file = importFiles(f);

        Input input = new InputFromFile(file);
        if (file.getName().endsWith(".xml")) {
            if (xmlparser == null) {
                throw new ParseException("XML parser not set");
            }
            xmlparser.setInput(input);
            IASTprogram program = xmlparser.getProgram();
            return program;
        }
        if (file.getName().endsWith(".ilpml")) {
            if (ilpmlparser == null) {
                throw new ParseException("ILPML parser not set");
            }
            ilpmlparser.setInput(input);
            IASTprogram program = ilpmlparser.getProgram();
            return program;
        }
        throw new ParseException("file extension not recognized");
    }

}
