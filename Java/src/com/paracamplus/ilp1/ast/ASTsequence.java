/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp1.ast;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTsequence;
import com.paracamplus.ilp1.interfaces.IASTvisitor;

public class ASTsequence extends ASTexpression implements IASTsequence {
    public ASTsequence (int line,IASTexpression[] expressions) {
    	this.line=line;
        this.expressions = expressions;
    }
    protected IASTexpression[] expressions;
    private int line;
    @Override
	public IASTexpression[] getExpressions() {
        return this.expressions;
    }

    @Override
	public <Result, Data, Anomaly extends Throwable> 
    Result accept(IASTvisitor<Result, Data, Anomaly> visitor, Data data)
            throws Anomaly {
        return visitor.visit(this, data);
    }

	@Override
	public int getLine() {
		return line;
	}
}
