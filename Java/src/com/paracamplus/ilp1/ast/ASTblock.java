/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp1.ast;

import com.paracamplus.ilp1.interfaces.IASTblock;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp1.interfaces.IASTvisitor;

public class ASTblock extends ASTexpression implements IASTblock {
    
    public static class ASTbinding extends AST implements IASTbinding {
        public ASTbinding (int line,IASTvariable variable, IASTexpression initialisation) {
            this.line=line;
            this.variable = variable;
            this.initialisation = initialisation;
        }
        private final IASTvariable variable;
        private final IASTexpression initialisation;
        private int line;
        @Override
		public IASTvariable getVariable () {
            return variable;
        }
        @Override
		public IASTexpression getInitialisation () {
            return initialisation;
        }
		@Override
		public int getLine() {
			return line;
		}
    }
    
    public ASTblock (int line,IASTbinding[] binding,
                     IASTexpression body ) {
    	this.line=line;
        this.binding = binding;
        this.body = body;
    }
    private final IASTbinding[] binding;
    private final IASTexpression body;
    private int line;
    @Override
	public IASTbinding[] getBindings() {
        return binding;
    }
    
    @Override
	public IASTexpression getBody() {
        return body;
    }
    public int getLine() {
    	return line;
    }

    @Override
	public <Result, Data, Anomaly extends Throwable> 
    Result accept(IASTvisitor<Result, Data, Anomaly> visitor, Data data)
            throws Anomaly {
        return visitor.visit(this, data);
    }
}
