/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp1.ast;

import com.paracamplus.ilp1.interfaces.IASTalternative;
import com.paracamplus.ilp1.interfaces.IASTbinaryOperation;
import com.paracamplus.ilp1.interfaces.IASTblock;
import com.paracamplus.ilp1.interfaces.IASTblock.IASTbinding;
import com.paracamplus.ilp1.interfaces.IASTfactory;
import com.paracamplus.ilp1.interfaces.IASTboolean;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTfloat;
import com.paracamplus.ilp1.interfaces.IASTinteger;
import com.paracamplus.ilp1.interfaces.IASTinvocation;
import com.paracamplus.ilp1.interfaces.IASToperator;
import com.paracamplus.ilp1.interfaces.IASTprogram;
import com.paracamplus.ilp1.interfaces.IASTsequence;
import com.paracamplus.ilp1.interfaces.IASTstring;
import com.paracamplus.ilp1.interfaces.IASTunaryOperation;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public class ASTfactory implements IASTfactory {

    @Override
	public IASTprogram newProgram(int line,IASTexpression expression) {
        return new ASTprogram(line,expression);
    }
    
    @Override
	public IASToperator newOperator(String name) {
        return new ASToperator(name);
    }

    @Override
	public IASTsequence newSequence(int line,IASTexpression[] asts) {
        return new ASTsequence(line,asts);
    }

    @Override
	public IASTalternative newAlternative(int line,
									      IASTexpression condition,
                                          IASTexpression consequence, 
                                          IASTexpression alternant) {
        return new ASTalternative(line,condition, consequence, alternant);
    }

    @Override
	public IASTvariable newVariable(String name) {
        return new ASTvariable(name);
    }


    @Override
	public IASTunaryOperation newUnaryOperation(int line,
											    IASToperator operator,
                                                IASTexpression operand) {
        return new ASTunaryOperation(line,operator, operand);
    }

    @Override
	public IASTbinaryOperation newBinaryOperation(int line,IASToperator operator,
            IASTexpression leftOperand, IASTexpression rightOperand) {
        return new ASTbinaryOperation(line,operator, leftOperand, rightOperand);
    }

    @Override
	public IASTinteger newIntegerConstant(String value) {
        return new ASTinteger(value); 
    }

    @Override
	public IASTfloat newFloatConstant(String value) {
        return new ASTfloat(value);
    }

    @Override
	public IASTstring newStringConstant(String value) {
        return new ASTstring(value);
    }

    @Override
	public IASTboolean newBooleanConstant(String value) {
        return new ASTboolean(value);
    }


    @Override
	public IASTblock newBlock(int line,
							  IASTbinding[] binding,
                              IASTexpression body) {
        return new ASTblock(line,binding, body);
    }
    @Override
	public IASTbinding newBinding(int line,IASTvariable variable, IASTexpression initialisation) {
        return new ASTblock.ASTbinding(line,variable, initialisation);
    }
    
    @Override
	public IASTinvocation newInvocation(int line,IASTexpression function,
            IASTexpression[] arguments) {
    	return new ASTinvocation(line,function, arguments);
    }

}
