/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp1.interpreter.operator;


import com.paracamplus.ilp1.interpreter.interfaces.EvaluationException;

public class Inequal extends BinaryOperator {
    
    public Inequal () {
        super("!=");
    }
    
    @Override
	public Object apply (Object arg1, Object arg2) 
            throws EvaluationException {
    	if ((arg1== null && arg2!=null)|| (arg1!= null && arg2==null) ) {
    		return(true);}
    	else {
    		if (arg1== null && arg2==null) {return false;  }
    		else {  	
    	 if ( arg1.getClass()==arg2.getClass()) {
   		  return !(arg1.toString().equals(arg2.toString()));
   		}
   		
   				 
   	  else{return !arg1.equals(arg2);}
        
    }}}
}
