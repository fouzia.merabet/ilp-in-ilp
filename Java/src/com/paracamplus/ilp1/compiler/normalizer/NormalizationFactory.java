/* *****************************************************************
 * ilp1 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp1
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp1.compiler.normalizer;

import java.util.concurrent.atomic.AtomicInteger;

import com.paracamplus.ilp1.ast.ASTalternative;
import com.paracamplus.ilp1.ast.ASTbinaryOperation;
import com.paracamplus.ilp1.ast.ASTboolean;
import com.paracamplus.ilp1.ast.ASTfloat;
import com.paracamplus.ilp1.ast.ASTinteger;
import com.paracamplus.ilp1.ast.ASToperator;
import com.paracamplus.ilp1.ast.ASTsequence;
import com.paracamplus.ilp1.ast.ASTstring;
import com.paracamplus.ilp1.ast.ASTunaryOperation;
import com.paracamplus.ilp1.compiler.CompilationException;
import com.paracamplus.ilp1.compiler.ast.ASTCblock;
import com.paracamplus.ilp1.compiler.ast.ASTCcomputedInvocation;
import com.paracamplus.ilp1.compiler.ast.ASTCglobalVariable;
import com.paracamplus.ilp1.compiler.ast.ASTClocalVariable;
import com.paracamplus.ilp1.compiler.ast.ASTCprogram;
import com.paracamplus.ilp1.compiler.interfaces.IASTCblock;
import com.paracamplus.ilp1.compiler.interfaces.IASTCcomputedInvocation;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalFunctionVariable;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalInvocation;
import com.paracamplus.ilp1.compiler.interfaces.IASTCglobalVariable;
import com.paracamplus.ilp1.compiler.interfaces.IASTClocalVariable;
import com.paracamplus.ilp1.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp1.compiler.interfaces.IASTCvariable;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASToperator;
import com.paracamplus.ilp1.interfaces.IASTvariable;
import com.paracamplus.ilp2.compiler.ast.ASTCglobalFunctionVariable;
import com.paracamplus.ilp2.compiler.ast.ASTCglobalInvocation;

public class NormalizationFactory
implements INormalizationFactory {
    
    public NormalizationFactory() {
        this.count = new AtomicInteger(0);
    }
    protected AtomicInteger count;
    
    @Override
	public IASTCprogram newProgram(int line,IASTexpression expression) {
        return new ASTCprogram(line,expression); 
    }

    // Various types of variables:
    
    @Override
	public IASTCvariable newVariable(String name) throws CompilationException {
        throw new CompilationException("Uncategorized variable " + name);
    }
   
    @Override
	public IASTClocalVariable newLocalVariable(String name) {
        String newName = name + count.incrementAndGet();
        return new ASTClocalVariable(newName);
    }
    @Override
	public IASTCglobalVariable newGlobalVariable(String name) {
        return new ASTCglobalVariable(name);
    }
   
   
    
    @Override
	public IASToperator newOperator(String name) {
        return new ASToperator(name);
    }
        
    
    @Override
	public IASTexpression newSequence(int line,IASTexpression[] asts) {
        return new ASTsequence(line,asts);
    }
    
    @Override
	public IASTexpression newAlternative(
			int line,
            IASTexpression condition,
            IASTexpression consequence, 
            IASTexpression alternant) {
        return new ASTalternative(line,condition, consequence, alternant);
    }
    
    // various types of invocation
    
    @Override
	public IASTexpression newInvocation(
			int line,
            IASTexpression function,
            IASTexpression[] arguments) throws CompilationException {
        throw new CompilationException("Uncategorized invocation ");
    }
    
    @Override
	public IASTexpression newUnaryOperation(
			int line,
            IASToperator operator,
            IASTexpression operand) {
        return new ASTunaryOperation(line,operator, operand);
    }
    
    @Override
	public IASTexpression newBinaryOperation(
			int line,
            IASToperator operator,
            IASTexpression leftOperand, 
            IASTexpression rightOperand) {
        return new ASTbinaryOperation(line,operator, leftOperand, rightOperand);
    }
    
    @Override
	public IASTexpression newIntegerConstant(String value) {
        return new ASTinteger(value);
    }
    
    @Override
	public IASTexpression newFloatConstant(String value) {
        return new ASTfloat(value);
    }
    
    @Override
	public IASTexpression newStringConstant(String value) {
        return new ASTstring(value);
    }
    
    @Override
	public IASTexpression newBooleanConstant(String value) {
        return new ASTboolean(value);
    }
    
    
    @Override
	public IASTCblock newBlock(int line,IASTCblock.IASTCbinding[] binding, IASTexpression body) {
        return new ASTCblock(line,binding, body);
    }
    @Override
	public IASTCblock.IASTCbinding newBinding(int line,IASTvariable variable, IASTexpression initialisation) {
        return new ASTCblock.ASTCbinding(line,variable, initialisation);
    }
    
    @Override
	public IASTCcomputedInvocation newComputedInvocation(
			int line,
            IASTexpression function,
            IASTexpression[] arguments) {
        return new ASTCcomputedInvocation(line,function, arguments);
    }
    
    @Override
	public IASTCglobalFunctionVariable newGlobalFunctionVariable(int line,String name) {
        return new ASTCglobalFunctionVariable(line,name);
    }
    
  
    @Override
	public IASTCglobalInvocation newGlobalInvocation(
			int line,
            IASTCglobalVariable function,
            IASTexpression[] arguments) {
        return new ASTCglobalInvocation(line,function, arguments);
    }

    
}
