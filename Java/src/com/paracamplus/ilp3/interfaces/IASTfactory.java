/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp3.interfaces;

import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp2.interfaces.IASTfunctionDefinition;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTfactory extends com.paracamplus.ilp2.interfaces.IASTfactory {
    @Override
	IASTprogram newProgram(
			int line,
    		IASTfunctionDefinition[] functions,
            IASTexpression expression);
    
    IASTexpression newTry (int line,
    					   IASTexpression body,
                           IASTlambda catcher,
                           IASTexpression finallyer );

    IASTlambda newLambda (int line,
    		              IASTvariable[] variables,
                          IASTexpression body );

    IASTnamedLambda newNamedLambda(
    		int line,
            IASTvariable functionVariable,
            IASTvariable[] variables,
            IASTexpression body );
    
    IASTexpression newCodefinitions(int line,
    								IASTnamedLambda[] functions,
                                    IASTexpression body);


}
