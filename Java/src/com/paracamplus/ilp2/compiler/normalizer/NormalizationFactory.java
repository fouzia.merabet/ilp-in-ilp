/* *****************************************************************
 * ilp2 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp2
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp2.compiler.normalizer;

import com.paracamplus.ilp2.ast.ASTassignment;
import com.paracamplus.ilp2.ast.ASTloop;
import com.paracamplus.ilp2.compiler.ast.ASTCfunctionDefinition;
import com.paracamplus.ilp2.compiler.ast.ASTCprogram;
import com.paracamplus.ilp2.compiler.interfaces.IASTCfunctionDefinition;
import com.paracamplus.ilp2.compiler.interfaces.IASTCprogram;
import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public class NormalizationFactory
extends com.paracamplus.ilp1.compiler.normalizer.NormalizationFactory
implements INormalizationFactory {
    
    @Override
	public IASTCprogram newProgram(
			int line,
            IASTCfunctionDefinition[] functions,
            IASTexpression expression) {
        return new ASTCprogram(line,functions,expression); 
    }
   
    @Override
	public IASTCfunctionDefinition newFunctionDefinition(
			int line,
            IASTvariable functionVariable,
            IASTvariable[] variables, 
            IASTexpression body) {
       return new ASTCfunctionDefinition(line,functionVariable, variables, body);
    }
    

    @Override
	public IASTexpression newAssignment(int line,
										IASTvariable variable,
                                        IASTexpression value) {
        return new ASTassignment(line,variable, value);
    }
    
    
    @Override
	public IASTexpression newLoop(int line,IASTexpression condition, IASTexpression body) {
        return new ASTloop(line,condition, body);
    }



}
