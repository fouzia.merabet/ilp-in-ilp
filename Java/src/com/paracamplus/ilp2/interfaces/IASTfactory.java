/* *****************************************************************
 * ILP9 - Implantation d'un langage de programmation.
 * by Christian.Queinnec@paracamplus.com
 * See http://mooc.paracamplus.com/ilp9
 * GPL version 3
 ***************************************************************** */
package com.paracamplus.ilp2.interfaces;


import com.paracamplus.ilp1.interfaces.IASTexpression;
import com.paracamplus.ilp1.interfaces.IASTvariable;

public interface IASTfactory extends com.paracamplus.ilp1.interfaces.IASTfactory {
    IASTprogram newProgram(
    		int line,
    		IASTfunctionDefinition[] functions,
            IASTexpression expression);
    
    IASTexpression newLoop(int line,
    					   IASTexpression condition,
                           IASTexpression body);

    IASTfunctionDefinition newFunctionDefinition(
    		int line,
            IASTvariable functionVariable,
            IASTvariable[] variables,
            IASTexpression body);
    
    IASTexpression newAssignment(
    		int line,
    		IASTvariable variable,
            IASTexpression value);
    
}
