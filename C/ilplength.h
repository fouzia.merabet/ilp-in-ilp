
/* $Id$ */

#ifndef ILP_LENGTH_H
#define ILP_LENGTH_H

#include <math.h>
#include "ilp.h"

extern ILP_Object ILP_length (ILP_Object o);

#endif /* ILP_LENGTH_H */

/* end of ilplength.h */
