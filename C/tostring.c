/* $Id$ */

#include "ilptostring.h"
//#include "ilpBasicError.h"
#include "ilp.h"

ILP_Object
ILP_tostring (ILP_Object o)
{
    if ( ! ILP_isInteger(o) )

        return ILP_make_String(o->_content.asString);
    else
      return ILP_domain_error("Not a string", o);
}

/* end of ilptostring.c */
