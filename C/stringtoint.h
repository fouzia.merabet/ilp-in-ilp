/* $Id$ */

#ifndef ILP_STRINGTOINT_H
#define ILP_STRINGTOINT_H

#include <math.h>
#include "ilp.h"

extern ILP_Object ILP_stringtoint (ILP_Object o);

#endif /* ILP_STRINGTOINT_H */

/* end of ilpstringtoint.h */
