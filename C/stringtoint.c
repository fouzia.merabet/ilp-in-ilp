
/* $Id$ */

#include "ilplstringtoint.h"
//#include "ilpBasicError.h"
#include "ilp.h"

ILP_Object
ILP_stringtoint (ILP_Object o)
{
    if ( ! ILP_isString(o) )
        return ILP_make_integer(atoi(o->_content.asString));
    else
      return ILP_domain_error("Not a string", o);
}

/* end of ilpstringtoint.c */