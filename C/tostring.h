/* $Id$ */

#ifndef ILP_TOSTRING_H
#define ILP_TOSTRING_H

#include <math.h>
#include "ilp.h"

extern ILP_Object ILP_tostring (ILP_Object o);

#endif /* ILP_TOSTRING_H */

/* end of tostring.h */
