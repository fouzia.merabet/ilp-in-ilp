#include <stdio.h>
#include <stdlib.h>
#include "ilp.h"

/* Global variables */
ILP_Object	print;

/* Global prototypes */

/* Global functions */


ILP_Object 
ilp_program()
{
	{
		ILP_Object	ilptmp382;
		{
			ILP_Object	ilptmp383;
			ilptmp383 = ILP_String2ILP("Un, ");
			ilptmp382 = ILP_print(ilptmp383);
		}
		{
			ILP_Object	ilptmp384;
			ilptmp384 = ILP_String2ILP("deux et ");
			ilptmp382 = ILP_print(ilptmp384);
		}
		{
			ILP_Object	ilptmp385;
			ilptmp385 = ILP_String2ILP("trois.");
			ilptmp382 = ILP_print(ilptmp385);
		}
		return ilptmp382;
	}

}

static ILP_Object 
ilp_caught_program()
{
	struct ILP_catcher *current_catcher = ILP_current_catcher;
	struct ILP_catcher new_catcher;

	if (0 == setjmp(new_catcher._jmp_buf)) {
		ILP_establish_catcher(&new_catcher);
		return ilp_program();
	};
	return ILP_current_exception;
}

int 
main(int argc, char *argv[])
{
	ILP_START_GC;
	ILP_print(ilp_caught_program());
	ILP_newline();
	return EXIT_SUCCESS;
}
