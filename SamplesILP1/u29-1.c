#include <stdio.h>
#include <stdlib.h>
#include "ilp.h"

/* Global variables */


ILP_Object 
ilp_program()
{
	{
		ILP_Object	ilptmp74;
		ilptmp74 = ILP_Integer2ILP(3);

		{
			ILP_Object	x1 = ilptmp74;
			{
				ILP_Object	ilptmp75;
				{
					ILP_Object	ilptmp76;
					ILP_Object	ilptmp77;
					ilptmp76 = x1;
					ilptmp77 = x1;
					ilptmp75 = ILP_Plus(ilptmp76, ilptmp77);
				}

				{
					ILP_Object	x2 = ilptmp75;
					{
						ILP_Object	ilptmp78;
						ILP_Object	ilptmp79;
						ilptmp78 = x2;
						ilptmp79 = x2;
						return ILP_Times(ilptmp78, ilptmp79);
					}

				}
			}

		}
	}

}

static ILP_Object 
ilp_caught_program()
{
	struct ILP_catcher *current_catcher = ILP_current_catcher;
	struct ILP_catcher new_catcher;

	if (0 == setjmp(new_catcher._jmp_buf)) {
		ILP_establish_catcher(&new_catcher);
		return ilp_program();
	};
	return ILP_current_exception;
}

int 
main(int argc, char *argv[])
{
	ILP_START_GC;
	ILP_print(ilp_caught_program());
	ILP_newline();
	return EXIT_SUCCESS;
}
