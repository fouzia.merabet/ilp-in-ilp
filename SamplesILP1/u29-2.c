#include <stdio.h>
#include <stdlib.h>
#include "ilp.h"

/* Global variables */


ILP_Object 
ilp_program()
{
	{
		ILP_Object	ilptmp80;
		ILP_Object	ilptmp81;
		ilptmp80 = ILP_Integer2ILP(11);
		ilptmp81 = ILP_Integer2ILP(22);

		{
			ILP_Object	x1 = ilptmp80;
			ILP_Object	y2 = ilptmp81;
			{
				ILP_Object	ilptmp82;
				ILP_Object	ilptmp83;
				{
					ILP_Object	ilptmp84;
					ILP_Object	ilptmp85;
					ilptmp84 = x1;
					ilptmp85 = y2;
					ilptmp82 = ILP_Plus(ilptmp84, ilptmp85);
				}
				{
					ILP_Object	ilptmp86;
					ILP_Object	ilptmp87;
					ilptmp86 = x1;
					ilptmp87 = y2;
					ilptmp83 = ILP_Times(ilptmp86, ilptmp87);
				}

				{
					ILP_Object	x3 = ilptmp82;
					ILP_Object	y4 = ilptmp83;
					{
						ILP_Object	ilptmp88;
						ILP_Object	ilptmp89;
						ilptmp88 = x3;
						ilptmp89 = y4;
						return ILP_Times(ilptmp88, ilptmp89);
					}

				}
			}

		}
	}

}

static ILP_Object 
ilp_caught_program()
{
	struct ILP_catcher *current_catcher = ILP_current_catcher;
	struct ILP_catcher new_catcher;

	if (0 == setjmp(new_catcher._jmp_buf)) {
		ILP_establish_catcher(&new_catcher);
		return ilp_program();
	};
	return ILP_current_exception;
}

int 
main(int argc, char *argv[])
{
	ILP_START_GC;
	ILP_print(ilp_caught_program());
	ILP_newline();
	return EXIT_SUCCESS;
}
